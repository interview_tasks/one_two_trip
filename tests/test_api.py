import requests
import pytest
import ujson
from faker import Faker

request_url = 'https://www.onetwotrip.com/_hotels/api/suggestRequest'
faker = Faker()


def test_smoke():
    """
    Test API actually works.
    """
    response = requests.post(request_url)
    assert response.status_code == 200


def test_no_errors():
    """
    Test API returns no errors.
    """
    response = requests.post(request_url)
    assert response.json()['error'] is None


@pytest.mark.parametrize('city_name, lang, locale, currency', [
    ('Moscow', 'ru', 'ru', 'RUB'),
    ('Moscow', 'en', 'en', 'USD'),
    ('Venice', 'fr', 'vi', 'EUR'),
    ('Berlin', 'de', 'de', 'EUR')
])
def test_cities(city_name, lang, locale, currency):
    """
    4 Tests with hardcode data.
    """
    data = {
        'query': city_name,
        'lang': lang,
        'locale': locale,
        'currency': currency,
    }

    response = requests.post(request_url, data=data)
    response = ujson.loads(response.text)
    cities_list = [a['city_slug'] for a in response['result']]
    assert city_name.lower() in cities_list


@pytest.mark.parametrize('country_slug', [faker.country()])
def test_country(country_slug):
    """
    Test with random country.
    """
    data = {'query': country_slug, }

    response = requests.post(request_url, data=data)
    response = ujson.loads(response.text)
    countries_list = [a['country_slug'] for a in response['result']]
    assert country_slug.lower() in countries_list
