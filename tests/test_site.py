import requests
import pytest

request_url = 'http://onetwotrip.com'
redirection_link = 'https://www.onetwotrip.com/'
status_codes = set(range(300, 309))


@pytest.fixture(scope='module')
def response():
    return requests.get(request_url)


def test_redirection_code(response):
    """
    Test redirection code.
    """
    status_codes_history = {ott.status_code for ott in response.history}

    assert status_codes_history.intersection(status_codes)


def test_redirection_url(response):
    """
    Test redirection url.
    """
    assert redirection_link in response.url
